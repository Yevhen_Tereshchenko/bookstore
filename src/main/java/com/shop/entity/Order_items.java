package com.shop.entity;

import java.util.Date;

public class Order_items {

    public int book_id;
    public int order_id;
    public int quantity;
    public Date created_at;
    public Date update_at;
}
