package com.shop.entity;

import java.util.Date;

public class Cards {

    public String card_number;
    public String name;
    public String image;
    public int cvv;
    public String expiration_month_year;
    public int user_id;
    public Date created_at;
    public Date update_at;


}
