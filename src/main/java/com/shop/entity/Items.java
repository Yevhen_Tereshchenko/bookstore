package com.shop.entity;

import java.util.Date;

public class Items {

    public int book_id;
    public int cart_id;
    public int quantity;
    public Date created_at;
    public Date update_at;
}
