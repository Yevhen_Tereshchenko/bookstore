package com.shop.entity;

import java.util.Date;

public class Addresses {

    public String address_type;
    public String first_name;
    public String last_name;
    public String address;
    public String city;
    public int zip;
    public String country;
    public String phone;
    public int user_id;
    public int checkbox_id;
    public int order_id;
    public Date created_at;
    public Date update_at;

}
