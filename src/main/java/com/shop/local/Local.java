package com.shop.local;

public class Local {

    String local;

    String fieldExitRU = "Выход";
    String fieldExitEN = "EXIT";

    String fieldTitleRU = "Наименование";
    String fieldTitleEN = "Title";

    String fieldAuthorRU = "Автор";
    String fieldAuthorEN = "Author";

    String fieldPriceRU = "Цена";
    String fieldPriceEN = "Price";

    String fieldYearRU = "Год";
    String fieldYearEN = "Year";

    String fieldNextPRU = "след стр";
    String fieldNextPEN = "next pages";


    String fieldAddtoCartRU = "Добавить";
    String fieldAddtoCartEN = "Add to Cart";


    public Local(String local) {
        this.local = local;
    }

    public String getLocal() {
        return local;
    }

    public String getFieldExit() {
        if (local.equals("ru")) {
            return fieldExitRU;
        } else if (local.equals("en")) {
            return fieldExitEN;
        }
        return "";
    }

    public String getFieldTitle() {
        if (local.equals("ru")) {
            return fieldTitleRU;
        } else if (local.equals("en")) {
            return fieldTitleEN;
        }
        return "";
    }

    public String getFieldAuthor() {
        if (local.equals("ru")) {
            return fieldAuthorRU;
        } else if (local.equals("en")) {
            return fieldAuthorEN;
        }
        return "";
    }

    public String getFieldPrice() {
        if (local.equals("ru")) {
            return fieldPriceRU;
        } else if (local.equals("en")) {
            return fieldPriceEN;
        }
        return "";
    }

    public String getFieldYear() {
        if (local.equals("ru")) {
            return fieldYearRU;
        } else if (local.equals("en")) {
            return fieldYearEN;
        }
        return "";
    }

    public String getFieldNextP() {
        if (local.equals("ru")) {
            return fieldNextPRU;
        } else if (local.equals("en")) {
            return fieldNextPEN;
        }
        return "";
    }

    public String getFieldAddtoCart() {
        if (local.equals("ru")) {
            return fieldAddtoCartRU;
        } else if (local.equals("en")) {
            return fieldAddtoCartEN;
        }
        return "";
    }
}
